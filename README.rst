OpenStack-Ansible os_swift_sync role
####################################

This Ansible role is no longer maintained or used.

The content of this role have been merged into the OpenStack-Ansible
os_swift role, rendering the inclusion of this additional role
superfluous.

For any further questions, please email
openstack-dev@lists.openstack.org or join #openstack-ansible on
Freenode.
